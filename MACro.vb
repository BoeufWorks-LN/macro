Sub MACro()
    Dim myRange As Range 'User provided data range
    Dim outp As Long 'User provided output format, check "Add the new separators"
    Dim customFormat As String 'User provided string containing the custom output format
    Dim cell As Range 'Current working cell
    Dim value As String 'Temp string containing the value of the current working cell
    
    'Continue on error
    On Error Resume Next
    
    'Require user to provide data range
    Set myRange = Application.InputBox("Please select a data range", "MACro - Select data", Selection.Address, , , , , 8)
    
    'Require user to provide format
    outFormat = Application.InputBox("Output format:" & vbCrLf & "1 -> xxxx.xxxx.xxxx" & vbCrLf & "2 -> xx-xx-xx-xx-xx-xx" & vbCrLf & "3 -> xxx.xxx.xxx.xxx" & vbCrLf & "4 -> xxxx xxxx xxxx" & vbCrLf & "5 -> xx xx xx xx xx xx" & vbCrLf & "6 -> XX:XX:XX:XX:XX:XX (DEFAULT)" & vbCrLf & "7 -> XXXXXXXXXXXX" & vbCrLf & "8 -> Specify format", "MACro - Select output format (uppercase only)", , , , , , 1)
    
    'If user selected custom format, then require it
    If outFormat = 8 Then
        customFormat = Application.InputBox("Enter the format, the @ symbolizes the number, keep in mind you need 12 @ for a full MAC address:" & vbCrLf & "ex: to get 'xxxx.xxxx.xxxx', you need to enter '@@@@.@@@@.@@@@'" & vbCrLf & "/!\ You cannot use '!' as a separator", "MACro - Insert custom format")
    End If
    
    'Exit if selected range is empty
    If myRange Is Nothing Then Exit Sub
    
    For Each cell In myRange
        value = cell.value
        
        'If the cell does not contain "nc" then whe can treat it as an address
        If (InStr(1, value, "nc", 1) = 0) And (Not IsEmpty(cell)) Then
        
            'Rid the MAC address from any of the common separators
            value = Replace(value, ".", "")
            value = Replace(value, ":", "")
            value = Replace(value, " ", "")
            value = Replace(value, "-", "")
            value = Replace(value, ",", "")
            
            'Convert value to proper case as defined by the format
            If outFormat >= 6 Then
                'Convert all chars from the address to uppercase
                value = UCase(value)
            Else
                'Convert all chars from the address to lowercase
                value = LCase(value)
            End If
            
            'Add the new separators
            Select Case outFormat
                Case 1
                    value = Format(Replace(value, " ", vbNullString), "@@@@.@@@@.@@@@")
                Case 2
                    value = Format(Replace(value, " ", vbNullString), "@@-@@-@@-@@-@@-@@")
                Case 3
                    value = Format(Replace(value, " ", vbNullString), "@@@.@@@.@@@.@@@")
                Case 4
                    value = Format(Replace(value, " ", vbNullString), "@@@@ @@@@ @@@@")
                Case 5
                    value = Format(Replace(value, " ", vbNullString), "@@ @@ @@ @@ @@ @@")
                Case 6
                    value = Format(Replace(value, " ", vbNullString), "@@:@@:@@:@@:@@:@@")
                Case 7
                    value = Format(Replace(value, " ", vbNullString), "@@@@@@@@@@@@")
                Case 8
                    value = Format(Replace(value, " ", vbNullString), customFormat)
                Case Else
                    value = Format(Replace(value, " ", vbNullString), "@@:@@:@@:@@:@@:@@")
            End Select
            
            'Write our string to the cell
            cell.value = value
            
        End If
        
    Next
End Sub
