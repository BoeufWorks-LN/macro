# MACro

## Presentation

__This repo contains the code for an excel macro that enables you to reformat mac addresses in Excel spreadsheets.__

## Supported output

Currently the reformatting can be done in the following standards:
* xxxx.xxxx.xxxx
* xx-xx-xx-xx-xx-xx
* xxx\.xxx\.xxx\.xxx
* xxxx xxxx xxxx
* xx xx xx xx xx xx
* XX:XX:XX:XX:XX:XX (DEFAULT)
* XXXXXXXXXXXX
* Any other format (USER PROMPT)

## Working procedure

1. Ask the user to select a data range
2. Ask the user to select the output standard
3. Check if the cell contains "nc" or is empty, if that is the case then skip the cell
4. If that is not the case then it gets rid of any of the following separators {'.', ':', ' ', '-', ','}
5. Convert the address to uppercase or lowercase according to the selected standard
6. Then finally it applies the new format with the selected standard

## License

__GNU GPLv3__
